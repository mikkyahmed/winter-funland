package com.example.flappybird;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

import java.util.ArrayList;
import java.util.Random;

public class GameManager {

    BgImage bgImage;
    FlyingSnowBall snowBall;
    static int gameState;
    ArrayList<TubeCollection>tubeCollections;
    Random rand;
    int scoreCount;
    int winningTube;
    Paint designPaint;

    public GameManager() {
        bgImage = new BgImage();
        snowBall = new FlyingSnowBall();
        gameState = 0;
        tubeCollections = new ArrayList<>();
        rand = new Random();
        generateTubeObjects();
        initScoreVariables();
    }

    public void initScoreVariables(){
        scoreCount = 0;
        winningTube = 0;
        designPaint = new Paint();
        designPaint.setColor(Color.BLUE);
        designPaint.setTextSize(250);
        designPaint.setStyle(Paint.Style.FILL);
        designPaint.setFakeBoldText(true);
        designPaint.setShadowLayer(5.0f, 20.0f, 20.0f, Color.WHITE);
    }


    public void generateTubeObjects(){
        for (int j = 0; j< AppHolder.tube_numbers; j++){
            int tubeX = AppHolder.SCRN_WIDTH_X + j *AppHolder.tubeDistance;
            int upTubeCollectionY = AppHolder.minimumTubeCollection_Y;
            rand.nextInt(AppHolder.maximumTubeCollection_Y - AppHolder.minimumTubeCollection_Y + 1);
            TubeCollection tubeCollection = new TubeCollection(tubeX, upTubeCollectionY);
            tubeCollections.add(tubeCollection);
        }
    }

    public void scrollingTube(Canvas can){
        if(gameState == 1){
            if ((tubeCollections.get(winningTube).getxTube() < snowBall.getX() + AppHolder.getbitmapControl().getBirdWidth())
             && (tubeCollections.get(winningTube).getUpTubeCollection_Y() > snowBall.getY()
                    || tubeCollections.get(winningTube).getDownTubeY() < (snowBall.getY() + AppHolder.getbitmapControl().getBirdHeight()))) {
                gameState = 2;
                AppHolder.getSoundPlay().playCrash();
                Context mContext = AppHolder.gameActivityContext;
                Intent mIntent = new Intent(mContext, GameOver.class);
                mIntent.putExtra("score", scoreCount);
                mContext.startActivity(mIntent);
                ((Activity)mContext).finish();
            }

            if (tubeCollections.get(winningTube).getxTube() < snowBall.getX() - AppHolder.getbitmapControl().getTubeWidth()){
                scoreCount ++;
                winningTube ++;
                AppHolder.getSoundPlay().playPing();
                if(winningTube > AppHolder.tube_numbers - 1){
                    winningTube = 0;
                }
            }
            for (int j = 0; j<AppHolder.tube_numbers; j++){
                if (tubeCollections.get(j).getxTube() < -AppHolder.getbitmapControl().getTubeWidth()){
                    tubeCollections.get(j).setxTube(tubeCollections.get(j).getxTube() + AppHolder.tube_numbers * AppHolder.tubeDistance);
                    int upTubeCollectionY = AppHolder.minimumTubeCollection_Y +
                            rand.nextInt(AppHolder.maximumTubeCollection_Y - AppHolder.minimumTubeCollection_Y + 1);
                    tubeCollections.get(j).setUpTubeCollection_Y(upTubeCollectionY);

                }
                tubeCollections.get(j).setxTube(tubeCollections.get(j).getxTube() - AppHolder.tubeVelocity);
                can.drawBitmap(AppHolder.getbitmapControl().getUpTube(), tubeCollections.get(j).getxTube(), tubeCollections.get(j).getUpTube_Y(), null);
                can.drawBitmap(AppHolder.getbitmapControl().getDownTube(),tubeCollections.get(j).getxTube(), tubeCollections.get(j).getDownTubeY(), null);

            }
            can.drawText("" + scoreCount, 450, 400, designPaint);
        }
    }


    public void snowAnimation(Canvas canvas){
        if(gameState == 1){
            if (snowBall.getY() < (AppHolder.SCRN_HEIGHT_Y - AppHolder.getbitmapControl().getBirdHeight()) || snowBall.getVelocity() < 0){
                snowBall.setVelocity(snowBall.getVelocity() + AppHolder.gravityPull);
                snowBall.setY(snowBall.getY() + snowBall.getVelocity()); }
        }
        int currentFrame = snowBall.getCurrentFrame();
        canvas.drawBitmap(AppHolder.getbitmapControl().getBird(currentFrame),snowBall.getX(),snowBall.getY(),null);
        currentFrame++;
        if(currentFrame > snowBall.maximumFrame){
            currentFrame=0;
        }
        snowBall.setCurrentFrame(currentFrame);
    }

    public void backgroundAnimation(Canvas canvas){
        bgImage.setX(bgImage.getX() - bgImage.getVelocity());
        if (bgImage.getX() < -AppHolder.getbitmapControl().getBackgroundWidth()){
            bgImage.setX(0);
        }
        canvas.drawBitmap(AppHolder.getbitmapControl().getBackground(),bgImage.getX(),bgImage.getY(),null);
        if (bgImage.getX() <- (AppHolder.getbitmapControl().getBackgroundWidth() - AppHolder.SCRN_WIDTH_X)){
            canvas.drawBitmap(AppHolder.getbitmapControl().getBackground(),
                    bgImage.getX() + AppHolder.getbitmapControl().getBackgroundWidth(), bgImage.getY(), null);
        }
    }

}
